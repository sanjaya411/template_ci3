<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UAC extends CR_Controller
{
  public function __construct()
  {
    parent::__construct();
    $protect_login = $this->Auth->protect_login();
    if ($protect_login->success === FALSE) {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Gagal!', '$protect_login->message')</script>");
      redirect("login");
      exit;
    }
  }

  public function view_uac_management()
  {
    $this->check_access("003U", TRUE);

    $data = [
      "list_access_control" => $this->core_model->get_position_list(),
      "list_division" => $this->core_model->get_division_list(),
      "allow_add_division" => $this->check_access("003UAD"),
      "allow_add_position" => $this->check_access("003UAP"),
    ];

    $this->view("admin/uac/v_index", "UAC Management", $data);
  }

  public function view_uac_detail($admin_tier_id)
  {
    $id = decrypt_url($admin_tier_id);
    $position = $this->core_model->get_position_detail($id);

    $data = [
      "position" => $position,
      "list_admin" => $this->core_model->get_user_by_tier($id),
      "allow_change_access" => $this->check_access("003UCHP")
    ];

    if ($data["allow_change_access"]) {
      $data["list_access"] = $this->core_model->get_access_tier($id);
    }

    $this->view("admin/uac/v_detail", $position->access_levelName, $data);
  }

  public function validate_position_add()
  {
    $this->check_access("003UAP", TRUE);

    $this->form_validation->set_rules('access_levelName', 'Position', 'required|alpha_numeric_spaces|is_unique[list_access_control.access_levelName]');
    $this->form_validation->set_rules('access_divisionId', 'Divisiion', 'required|alpha_numeric_spaces');

    if ($this->form_validation->run() === FALSE) {
      $form_error = $this->form_error_message();
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Failed!', '$form_error')</script>");
      redirect("/uac-management");
    } else {
      $this->add_position();
    }
  }

  private function add_position()
  {
    $input = (object) html_escape($this->input->post());
    $add_position = $this->core_model->add_position($input);

    $id = $add_position->id;
    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Success!', '$add_position->message')</script>");
    redirect("uac-management/$id");
  }

  public function process_position_assign($admin_tier_id)
  {
    $this->check_access("003UCHP", TRUE);

    $input = (object) html_escape($this->input->post());
    $input->admin_tier_id = $admin_tier_id;
    $check = $this->core_model->assign_access($input);
    if ($check->success) {
      $this->session->set_flashdata("pesan", "<script>sweet('success', 'Success!', '$check->message')</script>");
    } else {
      $this->session->set_flashdata("pesan", "<script>sweet('error', 'Failed!', '$check->message')</script>");
    }

    redirect("uac-management/$admin_tier_id");
  }

  public function validate_division_add()
  {
    $this->check_access("003UAD", TRUE);
    $input = (object) html_escape($this->input->post());
    $this->core_model->add_division($input);

    $this->session->set_flashdata("pesan", "<script>sweet('success', 'Success!', 'Success add new division!')</script>");
    redirect("uac-management");
  }
}